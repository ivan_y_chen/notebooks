import logging
import os
import pathlib


LOGGER = logging.getLogger(__name__)
if os.environ.get('DEBUG', '0') == '1':
    LOGGER.setLevel(logging.DEBUG)
else:
    LOGGER.setLevel(logging.INFO)
COURSE_TYPE = '_doc'


class SimilarSearcher:
    """
    A Class to find similar courses to the given ones
    """
    def __init__(self, es_client=None):
        """
        Parameters
        ----------
        es_client : elasticsearch.Elasticsearch
            a ES connection instance
        """
        self._es_client = es_client

    def search(self, courses, page_num=1, size=10, must_not=None):
        """Method to find similar courses to the given ones

        Parameters
        ----------
        courses : list
            A list of stings each of which is the agu code of a class.
        size : int
            The maximum number of items to return from the search engine.
        page_num : int
            The number of page
        must_not : dict or None
            Properties of courses that should not appear in the search results. Use to add additional business logics
            to the search results.
            The key should be one of the fields defined in the Elasticsearch mappings.
            The values should be a `list`
        Returns
        -------
        """
        query = SimilarQueryMaker().make_query(courses, size=size, page_num=page_num,must_not=must_not)
        return self._es_client.search(index=os.environ['COURSE_INDEX'], doc_type=COURSE_TYPE, body=query)

class SimilarQueryMaker:
    """Class to generate query in order to find similar courses to a list of given courses

    """
    def __init__(self):
        pass

    def _make_base_query(self, courses):
        courses = [{
            "_index": os.environ['COURSE_INDEX'],
            "_type": COURSE_TYPE,
            "_id": course_id
        } for course_id in courses]

        query = {
            "query": {
                "bool": {
                    "must": {
                        "dis_max": {
                            "queries": [
                                {
                                    "more_like_this": {
                                        "fields": ["lvl3skill", "title"],
                                        "like": courses,
                                        "boost_terms": 5,
                                        "min_term_freq": 1,
                                        "min_doc_freq": 3
                                    }
                                },
                                {
                                    "more_like_this": {
                                        "fields": ["lvl2skill", "description", "topics"],
                                        "like": courses,
                                        "boost_terms": 1,
                                        "min_term_freq": 1,
                                        "min_doc_freq": 3
                                    }
                                }
                            ]
                        }
                    },
                    "must_not": [
                        {
                            "term": {
                                "content_status": {
                                    "value": "inactive"
                                }
                            }
                        }
                    ]
                }
            }
        }
        return query

    def make_query(self, courses, page_num=1, size=10, must_not=None):
        """
        Parameters
        ----------
        courses : list
            A list of stings each of which is the agu code of a class.
        size : int
            The maximum number of items to return from the search engine.
        page_num : int
            The number of page
        must_not : dict or None
            Properties of courses that should not appear in the search results. Use to add additional business logics
            to the search results.
            The key should be one of the fields defined in the Elasticsearch mappings.
            The values should be a `list`
        Returns
        -------
        query : dict
           Request body that can be sent to Elasticsearch.
        """
        query = self._make_base_query(courses)
        if must_not is not None:
            if 'must_not' not in query['query']['bool']:
                query['query']['bool']['must_not'] = []
            for field, values in must_not.items():
                query['query']['bool']['must_not'].append({'terms': {field + '.key': values}})
        query['size'] = size
        query['from'] = size * (page_num - 1)
        return query