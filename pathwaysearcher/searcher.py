import logging
import os
import pathlib


LOGGER = logging.getLogger(__name__)
if os.environ.get('DEBUG', '0') == '1':
    LOGGER.setLevel(logging.DEBUG)
else:
    LOGGER.setLevel(logging.INFO)
COURSE_TYPE = '_doc'

class Searcher:
    """
    A class to perform course search on the Pathway
    """
    def __init__(self, es_client):
        """
        Parameters
        ----------
        es_client : elasticsearch.Elasticsearch
            a ES connection instance
        """
        self._es_client = es_client

    def search(self, user_query, is_auto_complete,
               must_not=None, filters=None, post_filters=None, size=10, page_num=1, active_courses_only=True):
        query = QueryMaker().make_query(user_query, is_auto_complete, must_not=must_not,
                                        filters=filters, post_filters=post_filters, size=size,
                                        page_num=page_num, active_courses_only=active_courses_only)
        # ES version < 7
        return self._es_client.search(index=os.environ['COURSE_INDEX'], doc_type=COURSE_TYPE, body=query)
        # # ES version >= 7
        # return self._es_client.search(index=os.environ['COURSE_INDEX'], body=query)

class QueryMaker:
    """
    A class to produce query for elasticsearch given an user query
    To use:
        query_maker = QueryMaker()
        # make a query for the autocompleting feature:
        query_maker.make_query(user_query, True)
        # make a query for the regular search
        query_maker.make_query(user_query, False)
        # make a query for browsing without any user query
        query_maker.make_query('', False, filters={'lvl1skill': ['Applied Processes']})
    """
    def __init__(self):
        pass
    def _make_search_query(self, user_query, active_courses_only=True):
        """Make a search query based on the user's query (`user_query`). Namely, the `query` part of the request body.

        The fields to search against included are: agu_code, title, topics, skills and description.

        Parameters
        ----------
        user_query : str
            Input from the user.
        active_courses_only : bool
            Whether just to return courses that are active
        Returns
        -------
        query : dict
            Request body that can be sent to Elasticsearch.
        """
        if not user_query:
            query = {
                'query': {
                    'bool': {
                        'should': [
                            {'match_all': {}}
                        ],
                        'must_not': [
                            {
                                "term": {
                                    "content_status": {
                                        "value": "inactive"
                                    }
                                }
                            }
                        ] if active_courses_only else []
                    }
                }
            }
            return query
        query = {
            'query': {
                'bool': {
                    'must_not': [
                        {
                            "term": {
                                "content_status": {
                                    "value": "inactive"
                                }
                            }
                        }
                    ] if active_courses_only else [],
                    'should': [
                        {
                            'term': {
                                'agu_code': {
                                    'value': user_query,
                                    'boost': 100
                                }
                            }
                        },
                        {
                            'multi_match': {
                                'query': user_query,
                                'fields': [
                                    'agu_code.prefixed',
                                    'agu_code.prefixed_without_digits'
                                ],
                                'type': 'most_fields'
                            }
                        },
                        {
                            'match_phrase': {
                                'processed_title': {
                                    'query': user_query,
                                    'boost': 100
                                }
                            }
                        },
                        {
                            'multi_match': {
                                'query': user_query,
                                'fields': ['processed_title', 'processed_title.bigrammed^5'],
                                'type': 'best_fields',
                                'boost': 10
                            }
                        },
                        {
                            'multi_match': {
                                'query': user_query,
                                'fields': ['topics', 'topics.bigrammed^5'],
                                'type': 'best_fields',
                                'boost': 5
                            }
                        },
                        {
                            'multi_match': {
                                'query': user_query,
                                'fields': ['lvl1skill', 'lvl1skill.bigrammed^5'],
                                'type': 'best_fields',
                                'boost': 50
                            }
                        },
                        {
                            'multi_match': {
                                'query': user_query,
                                'fields': ['lvl2skill', 'lvl2skill.bigrammed^5'],
                                'type': 'best_fields',
                                'boost': 50
                            }
                        },
                        {
                            'multi_match': {
                                'query': user_query,
                                'fields': ['lvl3skill', 'lvl3skill.bigrammed^5'],
                                'type': 'best_fields',
                                'boost': 50
                            }
                        },
                        {
                            'multi_match': {
                                'query': user_query,
                                'fields': ['processed_title',
                                           'processed_title.bigrammed^5',
                                           'topics',
                                           'topics.bigrammed^5',
                                           'lvl1skill',
                                           'lvl1skill.bigrammed^5',
                                           'lvl2skill',
                                           'lvl2skill.bigrammed^5',
                                           'lvl3skill',
                                           'lvl3skill.bigrammed^5',
                                           'description',
                                           'description.bigrammed^5',
                                           'description.trigrammed^10',
                                           'agu_code.prefixed',
                                           'agu_code.prefixed_without_digits',
                                           'platform.analyzed^50'],
                                'type': 'cross_fields',
                                'tie_breaker': 1
                            }
                        }
                    ],
                    'minimum_should_match': 2
                }
            },
            'explain': True
        }
        return query

    def _make_auto_com_query(self, user_query):
        """Make a query for the auto completion feature based on the user's query (`user_query`).

        The fields that used to provide data for the auto completion feature are: title, agu_code, topics and skills.

        Parameters
        ----------
        user_query : str
            Input from the user.

        Returns
        -------
        query : dict
            Request body that can be sent to Elasticsearch.
        """
        query = {
            'suggest': {
                'title_auto_com': {
                    'prefix': user_query,
                    'completion': {
                        'field': 'title_auto_com',
                        'size': 3,
                        'skip_duplicates': True,
                    }
                },
                'agu_code_auto_com': {
                    'prefix': user_query,
                    'completion': {
                        'field': 'agu_code_auto_com',
                        'size': 3,
                        'skip_duplicates': True,
                    }
                },
                'skill_auto_com': {
                    'prefix': user_query,
                    'completion': {
                        'field': 'skill_auto_com',
                        'size': 3,
                        'skip_duplicates': True,
                    }
                },
                'topic_auto_com': {
                    'prefix': user_query,
                    'completion': {
                        'field': 'topic_auto_com',
                        'size': 3,
                        'skip_duplicates': True,
                    }
                }
            }
        }
        return query

    def _make_did_you_mean_query(self, user_query):
        """Make a query for the search query suggestion feature based on the user's query (`user_query`).

        The query requests for search query suggestion based on four of the fields of the documents in ES, agu_code,
        title, topics and skills. The suggested words have the following properties:
            - (Share the same prefix (longer than 3 characters) with the word entered by the user. OR
            - Share the same suffix (longer than 3 characters) with the word entered by the user.) AND
            - Edit distance between the word entered by the user and the suggested word is no more than 2.

        Parameters
        ----------
        user_query : str
            Input from the user.

        Returns
        -------
        query : dict
            Request body that can be sent to Elasticsearch.
        """
        query = {
            'suggest': {
                'text': user_query,
                'agu_code_did_you_mean': {
                    'phrase': {
                        'field': 'agu_code.did_you_mean',
                        'size': 3,
                        'direct_generator': [
                            {
                                'field': 'agu_code.did_you_mean',
                                'suggest_mode': 'always',
                                'min_word_length': 3,
                                'prefix_length': 3
                            },
                            {
                                'field': 'agu_code.did_you_mean_reversed',
                                'suggest_mode': 'always',
                                'pre_filter': 'did_you_mean_agu_code_reversed',
                                'min_word_length': 3,
                                'prefix_length': 3
                            }
                        ],
                        'collate': {
                            'query': {
                                'source': {
                                    'term': {
                                        'agu_code': {
                                            'value': '{{suggestion}}'
                                        }
                                    }
                                }
                            },
                            'prune': False
                        },
                        'smoothing': {
                            "stupid_backoff": {
                                "discount": 0.4
                            }
                        }
                    }
                },
                'title_did_you_mean': {
                    'phrase': {
                        'field': 'processed_title.with_stopwords',
                        'size': 3,
                        'direct_generator': [
                            {
                                'field': 'processed_title.with_stopwords',
                                'suggest_mode': 'always',
                                'min_word_length': 3,
                                'prefix_length': 3
                            },
                            {
                                'field': 'processed_title.reversed',
                                'suggest_mode': 'always',
                                'pre_filter': 'reversed',
                                'post_filter': 'reversed',
                                'min_word_length': 3,
                                'prefix_length': 3
                            }
                        ],
                        'collate': {
                            'query': {
                                'source': {
                                    'match': {
                                        'processed_title.with_stopwords': '{{suggestion}}'
                                    }
                                }
                            },
                            'prune': False
                        },
                        'smoothing': {
                            "stupid_backoff": {
                                "discount": 0.4
                            }
                        }
                    }
                },
                'topics_and_skills_did_you_mean': {
                    'phrase': {
                        'field': 'topics_and_skills.did_you_mean',
                        'size': 3,
                        'direct_generator': [
                            {
                                'field': 'topics_and_skills.did_you_mean',
                                'suggest_mode': 'always',
                                'min_word_length': 3,
                                'prefix_length': 3
                            },
                            {
                                'field': 'topics_and_skills.did_you_mean_reversed',
                                'suggest_mode': 'always',
                                'pre_filter': 'reversed',
                                'post_filter': 'reversed',
                                'min_word_length': 3,
                                'prefix_length': 3
                            }
                        ],
                        'collate': {
                            'query': {
                                'source': {
                                    'match': {
                                        'topics_and_skills': '{{suggestion}}'
                                    }
                                }
                            },
                            'prune': False
                        },
                        'smoothing': {
                            "stupid_backoff": {
                                "discount": 0.4
                            }
                        }
                    }
                }
            }
        }
        return query

    def _make_agg_clause(self, agg_category):
        if agg_category == 'content_format':
            clause = {
                'content_format': {
                    'terms': {
                        'field': 'content_format'
                        }
                    }
                }
        elif agg_category == 'platform':
            clause = {
                'platform': {
                    'terms': {
                        'field': 'platform'
                    }
                }
            }
        elif agg_category == 'content_length_range':
            clause = {
                'content_length_range': {
                    'range': {
                        'field': 'content_length',
                        'keyed': True,
                        'ranges': [
                            {'key': '< 1 hour', 'to': 1.0},
                            {'key': '1 - 5 hours', 'from': 1.0, 'to': 5.0},
                            {'key': '5+ hours', 'from': 5.0}
                        ]
                    }
                }
            }
        elif agg_category == 'price':
            clause = {
                'price': {
                    'range': {
                        'field': 'price',
                        'keyed': True,
                        'ranges': [
                            {'key': 'FREE', 'to': 1.0},
                            {'key': '$1 - $100', 'from': 1.0, 'to': 100.0},
                            {'key': '$100+', 'from': 100.0}
                        ]
                    }
                }
            }
        return clause

    def _make_filter_clause_helper(self, filters):
        if not filters:
            return []
        filter_clauses = []
        for filter_, values in filters.items():
            if filter_ == 'content_length_range':
                cur_filter_clause = {'bool': {'should': []}}
                for value in values:
                    if value == '< 1 hour':
                        cur_filter_clause['bool']['should'].append({'range': {'content_length': {'lt': 1.0}}})
                    elif value == '1 - 5 hours':
                        cur_filter_clause['bool']['should'].append({'range': {'content_length': {'gte': 1.0, 'lt': 5.0}}})
                    else:
                        cur_filter_clause['bool']['should'].append({'range': {'content_length': {'gte': 5.0}}})

            elif filter_ == 'price':
                cur_filter_clause = {'bool': {'should': []}}
                for value in values:
                    if value == 'FREE':
                        cur_filter_clause['bool']['should'].append({'range': {'price': {'lt': 1.0}}})
                    elif value == '$1 - $100':
                        cur_filter_clause['bool']['should'].append({'range': {'price': {'gte': 1.0, 'lt': 100.0}}})
                    else:
                        cur_filter_clause['bool']['should'].append({'range': {'price': {'gte': 100.0}}})
            elif filter_ in {'lvl1skill', 'lvl2skill', 'lvl3skill', 'agu_code'}:
                cur_filter_clause = {'terms': {filter_ + '.key': values}}
            else:
                cur_filter_clause = {'terms': {filter_: values}}

            filter_clauses.append(cur_filter_clause)
        return filter_clauses

    def _make_filter_clauses(self, filters):
        return self._make_filter_clause_helper(filters)

    def _make_must_not_clauses(self, must_not):
        return self._make_filter_clause_helper(must_not)

    def _make_post_filter_clauses(self, post_filters):
        return self._make_filter_clause_helper(post_filters)

    def _make_aggregation_query(self, post_filters):
        """Make a query for count aggregations of the search results.

        Counts are generated for these fields: format, platform and content length.

        Returns
        -------
        query : dict
            Request body that can be sent to Elasticsearch.
        """
        if not post_filters:
            query = {
                'aggs': {
                    **self._make_agg_clause('content_format'),
                    **self._make_agg_clause('platform'),
                    **self._make_agg_clause('content_length_range'),
                    **self._make_agg_clause('price')
                }
            }
        elif 'content_format' in post_filters:
            query = {
                'aggs': {
                    **self._make_agg_clause('content_format')
                }
            }
        elif 'platform' in post_filters:
            query = {
                'aggs': {
                    **self._make_agg_clause('platform')
                }
            }
        elif 'content_length_range' in post_filters:
            query = {
                'aggs': {
                    **self._make_agg_clause('content_length_range')
                }
            }
        elif 'price' in post_filters:
            query = {
                'aggs': {
                    **self._make_agg_clause('price')
                }
            }
        return query

    def _make_sub_aggregation_query(self, post_filters):
        """Make a query for sub count aggregations of the search results.

        post_filters : dict
            The acceptable keys are `content_format`, `platform`, `content_length_range`.
            Value of a key is a `list`.

        Returns
        -------
        query : dict
            Request body that can be sent to Elasticsearch.
        """
        if not post_filters:
            query = {}
            return query

        query = {
            'sub_aggs': {
                'filter': {
                    'bool': {
                        'filter': self._make_post_filter_clauses(post_filters)
                    }
                }
            }
        }
        if 'content_format' in post_filters:
            query['sub_aggs']['aggs'] = {
                **self._make_agg_clause('platform'),
                **self._make_agg_clause('content_length_range'),
                **self._make_agg_clause('price')
            }

        elif 'platform' in post_filters:
            query['sub_aggs']['aggs'] = {
                **self._make_agg_clause('content_format'),
                **self._make_agg_clause('content_length_range'),
                **self._make_agg_clause('price')
            }
        elif 'content_length_range' in post_filters:
            query['sub_aggs']['aggs'] = {
                    **self._make_agg_clause('content_format'),
                    **self._make_agg_clause('platform'),
                    **self._make_agg_clause('price')
            }
        elif 'price' in post_filters:
            query['sub_aggs']['aggs'] = {
                    **self._make_agg_clause('content_format'),
                    **self._make_agg_clause('platform'),
                    **self._make_agg_clause('content_length_range'),
            }
        return query

    def _make_post_filter_query(self, post_filters):
        """Make a query for the post filter feature.

        Post filter is used to narrow down results after a search.

        Parameters
        ----------
        post_filters : dict
            The acceptable keys are `content_format`, `platform`, `content_length_range`.
            Value of a key is a `list`.

        Returns
        -------
        query : dict
           Request body that can be sent to Elasticsearch.
        """
        if not post_filters:
            return {}

        for filter_ in post_filters:
            if filter_ not in {'content_format', 'platform', 'content_length_range', 'price'}:
                raise KeyError(f'{filter} is an unexpected key')

        query = {
            'post_filter': {
                'bool': {
                    'must': [
                    ]
                }
            }
        }
        post_filter_clauses = self._make_post_filter_clauses(post_filters)
        if post_filter_clauses:
            query['post_filter']['bool']['must'] = post_filter_clauses

        return query

    def make_query(self, user_query, is_auto_complete,
                   must_not=None, filters=None, post_filters=None, size=10, page_num=1, active_courses_only=True):
        """Make query for course searching.

        Parameters
        ----------
        user_query : str
            Input from the user.
        is_auto_complete : bool
            Whether or not the query is for the auto-completion feature.
        must_not : dict or None
            Properties of courses that should not appear in the search results. Use to add additional business logics
            to the search results.
            The key should be one of the fields defined in the Elasticsearch mappings.
            The values should be a `list`
        filters : dict or None
            Properties of courses that should appear in the search results. Use to add additional business logics
            to the search results.
            The key should be one of the fields defined in the Elasticsearch mappings.
            The values should be a `list`
        post_filters : dict or None
            The acceptable keys are `content_format`, `platform`, `content_length_range`, `price`.
            Value of a key is a `list`.
        size : int
            The maximum number of items to return from the search engine.
        page_num : int
            The number of page
        active_courses_only : bool
            Whether just to return courses that are active
        Returns
        -------
        query : dict
           Request body that can be sent to Elasticsearch.
        """
        if is_auto_complete:
            return self._make_auto_com_query(user_query)

        search_query = self._make_search_query(user_query, active_courses_only=active_courses_only)
        did_you_mean_query = self._make_did_you_mean_query(user_query)
        aggregation_query = self._make_aggregation_query(post_filters)
        sub_aggregation_query = self._make_sub_aggregation_query(post_filters)
        if sub_aggregation_query:
            aggregation_query['aggs'] = {**aggregation_query['aggs'], **sub_aggregation_query}
        query = {**search_query, **did_you_mean_query, **aggregation_query}
        if post_filters is not None:
            post_filter_query = self._make_post_filter_query(post_filters)
            query.update(post_filter_query)
        query['size'] = size
        query['from'] = size * (page_num - 1)
        query['query']['bool']['filter'] = \
            query['query']['bool'].get('filter', []) + self._make_filter_clauses(filters)
        query['query']['bool']['must_not'] = \
            query['query']['bool'].get('must_not', []) + self._make_must_not_clauses(must_not)

        return query