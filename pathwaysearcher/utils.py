import logging
import os

import boto3
from elasticsearch import Elasticsearch, NotFoundError, RequestsHttpConnection
from elasticsearch.client import IndicesClient
from requests_aws4auth import AWS4Auth


LOGGER = logging.getLogger(__name__)
if os.environ.get('DEBUG', '0') == '1':
    LOGGER.setLevel(logging.DEBUG)
else:
    LOGGER.setLevel(logging.INFO)


def connect_es(env=None):
    """Function to establish a connection to ES for the PATHWAY platform based on the env setting.

    Returns
    -------

    """
    env = os.environ.get('ENV') if env is None else env
    if env not in {'prod', 'test'}:
        raise KeyError(f'Environment variable `ENV` is not Valid. Got {env}.')
    if env != 'prod':
        LOGGER.debug('Connecting to ES in the test env...')
        session = boto3.session.Session()
        credentials = session.get_credentials()
        awsauth = AWS4Auth(credentials.access_key,
                           credentials.secret_key,
                           session.region_name,
                           'es',
                           session_token=credentials.token)
        es = Elasticsearch(
            hosts=[os.environ.get('ES_HOST')],
            http_auth=awsauth,
            use_ssl=True,
            verify_certs=True,
            connection_class=RequestsHttpConnection
        )
        LOGGER.debug(f"Connection established! Host IP: {os.environ.get('ES_HOST')}")
    else:
        LOGGER.debug('Connecting to ES in the prod env...')
        es = Elasticsearch(
            hosts=[{'host': os.environ.get('ES_HOST'),
                    'port': os.environ.get('ES_PORT')}],
        )
        LOGGER.debug(f"Connection established! Host IP: {os.environ.get('ES_HOST')}")
    return es

def update_mappings(es_conn, index, mappings, settings):
    """Function to update mappings and settings of an index. If the index does not exist, it will create one.

    Parameters
    ----------
    es_conn : elasticsearch.Elasticsearch
        A connection to elasticsearch
    index : str
        An index name
    mappings : dict
        A new mappings to be applied
    settings : dict
        A new settings to be applied

    Returns
    -------
    status: dict

    """
    ind_client = IndicesClient(es_conn)
    new_mappings = {
        'mappings': mappings,
        'settings': settings
    }
    try:
        ind_client.delete(index)
    except NotFoundError:
        LOGGER.warning(f'Index {index} does not exist. Creating index.')
        pass
    status = ind_client.create(index, body=new_mappings)
    return status