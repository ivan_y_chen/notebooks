import logging
import os
from pathwaysearcher.searcher import Searcher
from pathwaysearcher.similarity_searcher import SimilarSearcher
from pathwaysearcher.post_processor import PostProcessor
from pathwaysearcher.utils import connect_es

LOGGER = logging.getLogger(__name__)
if os.environ.get('DEBUG', '0') == '1':
    LOGGER.setLevel(logging.DEBUG)
else:
    LOGGER.setLevel(logging.INFO)

class PathwayCourseFinder:
    """`PathwayCourseFinder` class will perform search, do post-processing and return the search results that will
    directly be used by the frontend.

    """
    def __init__(self):
        self._courses_to_include = set()
        self._courses_to_exclude = set()

    def add_courses_to_include(self, courses):
        """Method to add courses to the search results no matter whether they match the user's query or not.

        Parameters
        ----------
        courses : list
            A list of strings each of which is the agu code of a class.

        Returns
        -------

        """
        for course in courses:
            self._courses_to_include.add(course)

    def add_courses_to_exclude(self, courses):
        """Method to exclude courses from the search results no matter whether they match the user's query or not.

        Parameters
        ----------
        courses : list
            A list of strings each of which is the agu code of a class.

        Returns
        -------

        """
        for course in courses:
            self._courses_to_exclude.add(course)

    def search(self, user_query, is_auto_complete, must_not=None, filters=None,
               post_filters=None, size=10, page_num=1, active_courses_only=True):
        """Search courses

        Parameters
        ----------
        user_query : str
            Input from the user.
        is_auto_complete : bool
            Whether or not the query is for the auto-completion feature.
        must_not : dict or None
            Properties of courses that should not appear in the search results. Use to add additional business logics
            to the search results.
            The key should be one of the fields defined in the Elasticsearch mappings.
            The values should be a `list`
        filters : dict or None
            Properties of courses that should appear in the search results. Use to add additional business logics
            to the search results.
            The key should be one of the fields defined in the Elasticsearch mappings.
            The values should be a `list`
        post_filters : dict or None
            The acceptable keys are `content_format`, `platform`, `content_length_range`.
            Value of a key is a `list`.
        size : int
            The maximum number of items to return from the search engine.
        page_num : int
            The number of page
        active_courses_only : bool
            Whether just to return courses that are active
        Returns
        -------
        dict
        """
        if self._courses_to_include and filters is not None:
            filters['agu_code'] = filters.get('agu_code', []).extend(self._courses_to_include)
        elif self._courses_to_include:
            filters = {}
            filters['agu_code'] = list(self._courses_to_include)

        if self._courses_to_exclude and must_not is not None:
            must_not['agu_code'] = must_not.get('agu_code', []).extend(self._courses_to_exclude)
        elif self._courses_to_exclude:
            must_not = {}
            must_not['agu_code'] = list(self._courses_to_exclude)

        es_conn = connect_es()
        searcher = Searcher(es_conn)
        es_res = searcher.search(user_query, is_auto_complete, must_not=must_not, filters=filters,
                    post_filters=post_filters, size=size, page_num=page_num, active_courses_only=active_courses_only)
        search_results = PostProcessor().process(es_res, is_auto_complete)

        if not is_auto_complete:
            return search_results

        if any(vals for vals in search_results.values()) or user_query[:-1] == '':
            return search_results

        for prefix_len_reduction in range(1, 3):
            es_res = searcher.search(user_query[:-prefix_len_reduction], True)
            search_results = PostProcessor().process(es_res, True)
            if any(vals for vals in search_results.values()) or user_query[:-prefix_len_reduction-1] == '':
                break
        return search_results

    def find_similar_courses(self, courses, page_num=1, size=10, must_not=None):
        """Method to find similar courses to the given ones

        Parameters
        ----------
        courses : list
            A list of stings each of which is the agu code of a class.
        size : int
            The maximum number of items to return from the search engine.
        page_num : int
            The number of page
        must_not : dict or None
            Properties of courses that should not appear in the search results. Use to add additional business logics
            to the search results.
            The key should be one of the fields defined in the Elasticsearch mappings.
            The values should be a `list`
        Returns
        -------
        es_res : search results from ES
        """
        es_conn = connect_es()
        searcher = SimilarSearcher(es_conn)
        es_res = searcher.search(courses, size=size, page_num=page_num, must_not=must_not)
        return es_res